dsmci

a script for backup up large filesysteme with parallel tree walks and backup jobs
using intially IBM TSM / ISP and the "dsmc i" command

there are two versions of the script
- `dsmci.pl` has passed some tests and is in use at GWDG
- `dev-dsmci.pl` contains some new functions, but is in development

**Important:**

The newer versions of dsmci.pl uses the module "File::Find::Rule". This seems not to be part of all perl installations so
you may need to install it manually like this
  cpan -i File::Find::Rule

**Even if `dsmci.pl` has passed some tests and is in use, we do not assume no guarantee or liability for errors or omissions.**

Contributions are welcome :-)

*former develepment:*

the project started as a collection of scripts, initially there were three flavours
- bash       (for Linux only, deprecated, abandomed)
- perl       (for Linux and Windows, in develeopment)
- powershell (for windows only, abandomed)

then there were also different versions inside the perl implementation
- dsmcis.pl     the simple approach working just one level below the given start folder, "S" is for "simple"
- dsmci.pl      an approache allowing to set the depth to dive in before doing the parallel processing
- dsmciFD.pl    an approche diving down to the deepest folder level and backup up *all* folders in parallel, "FD" is for "full depth"


