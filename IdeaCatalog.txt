# a scratchpad for sharing ideas to improve "dsmci{|s|FD}"

# NY-2017-10-25
What about recursion for deep diving backups?
PROs:
+ simple implementation

CONs:
- how limit number of threads especially if the folders aren't well balanced?


# NY-2018-02-06
o using different path to start, e.g. multiple shares
o check given path for masquerading and remove uncesssary >"<
=> all done within a couple of hours :-D

# NY-2018-02-16
some ideas on profiling for "deeper" parallelization
- profile should know if the folder was backed up with (SUY) or without (SUN) subfolders
=> maybe put an mark in every profiling line, e.g. adjust the pattern to
time ; path ; <SUN|SUY>??
!! as there might be a ":" within the path ";" is taken as delimiter
=> done!

# NY-2018-07-17
- add a control to switch between the three versions?
- might simplify the "whole" code as all three cannot differ in main parts?